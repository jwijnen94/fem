""" Module: IOlib
def readGmsh2D():
"""
# ------------------------------------------------------------------------------

from sys import stdout
from numpy import array, unique

from FEM.FElib import Node, Element, ElementTF, Mesh

# ------------------------------------------------------------------------------

def readGmsh(filename, materials, disp, force, traction):
    print("{:25}".format("Reading mesh..."), end=""); stdout.flush()

    with open(filename) as file:

        #########
        # Nodes #
        #########

        line = file.readline().strip()

        # find `$Nodes` keyword
        while line != "$Nodes":
            line = file.readline().strip()

        # Nr of nodes
        nnodes = int(file.readline())

        # Create nodes
        nodes = []
        idof = 0
        for inode in range(nnodes):
            line = file.readline().split()

            x = array([float(line[1]), float(line[2])], dtype=float)
            dofs = array([idof, idof+1])
            idof += 2

            nodes.append(Node(inode, x, dofs))

        ###########################
        # Elements and boundaries #
        ###########################

        # Find `$Elements` keyword
        while line != "$Elements":
            line = file.readline().strip()

        # Nr of elements and boundaries
        nelems = int(file.readline())

        # Create nodes and assemble boundary arrays
        elems = []
        ielem = 0
        u_bc = []
        f_bc = []
        t_bc = []
        current_t = 0
        for key in range(nelems):
            line = file.readline().split()
            elem_type = int(line[1])
            ntags = int(line[2])
            node_ids = array(line, dtype=int)[3+ntags:] -1

            # Check if element is boundary
            if elem_type == 1 or elem_type == 8 or elem_type == 15:
                bc_id = int(line[3])

                # Prescribed displacements
                if bc_id in disp:
                    cond = disp[bc_id]
                    for node_id in node_ids:
                        dofs = nodes[node_id].dofs
                        for jdof, bc in enumerate(cond):
                            if bc is not None:
                                u_bc.append([dofs[jdof], bc])

                # Applied forces
                if bc_id in force:
                    cond = force[bc_id]
                    for node_id in node_ids:
                        dofs = nodes[node_id].dofs
                        for jdof, bc in enumerate(cond):
                            f_bc.append([dofs[jdof], bc])

                # Applied tractions
                if bc_id in traction:
                    cond = traction[bc_id]
                    if elem_type == 1: # 1st order line
                        t_bc.append([node_ids[0], node_ids[1], cond[0], cond[1]])
                    elif elem_type == 8: # 2nd order line
                        t_bc.append([node_ids[0], node_ids[1], node_ids[2], cond[0], cond[1]])


            # Elements
            else:
                elem_nodes = [nodes[i] for i in node_ids]
                elems.append(Element(ielem, elem_nodes, elem_type, materials[int(line[3])]))
                ielem += 1

    # Convert to ndarray
    u_bc = array(u_bc)
    f_bc = array(f_bc)
    t_bc = array(t_bc)

    # Filter double prescribed displacements
    trash, keys = unique(u_bc[:,0], return_index=True)
    u_bc = u_bc[keys]

    bc = {
        'disp': u_bc,
        'force': f_bc,
        'traction': t_bc
    }


    ########
    # Mesh #
    ########
    mesh = Mesh(elems, nodes, idof)

    print("Done")
    print("{} elements, {} nodes, {} dofs\n".format(len(elems), len(nodes), len(nodes)*2)); stdout.flush()

    return mesh, bc
