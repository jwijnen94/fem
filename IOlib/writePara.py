
from sys import stdout
from numpy import mean

def writePara(filename, mesh, model, u, f):
    print("{:25}".format("Writing output..."), end=''); stdout.flush()

    with open(filename, 'w') as file:

        file.writelines(["# vtk DataFile Version 3.0\n",
                         "vtk output\n",
                         "ASCII\n",
                         "DATASET UNSTRUCTURED_GRID\n"])


        ############
        # Geometry #
        ############

        file.write("POINTS {} float\n".format(len(mesh.nodes)))
        for node in mesh.nodes:
            file.write("{} {} {}\n".format(node.x[0], node.x[1], 0))

        file.write("CELLS {} {}\n".format(len(mesh), (len(mesh[0])+1)*len(mesh)))
        for elem in mesh:
            file.write("{}".format(len(elem)))
            for node in elem.nodes:
                file.write(" {}".format(node.ID))
            file.write("\n")

        file.write("CELL_TYPES {}\n".format(len(mesh.elems)))
        for elem in mesh:
            if elem.type == 2:
	            file.write("5\n")
            elif elem.type == 3:
                file.write("9\n")
            elif elem.type == 10 or elem.type == 16 or elem.type == 116:
                file.write("23\n")


        ########
        # Data #
        ########

        file.writelines(["POINT_DATA {}\n".format(len(mesh.nodes)),
                         "VECTORS disp float\n"])
        for node in mesh.nodes:
            dofs = node.dofs
            file.write("{} {} {}\n".format(u[dofs[0]], u[dofs[1]], 0))

        file.write("VECTORS force float\n")
        for node in mesh.nodes:
            dofs = node.dofs
            file.write("{} {} {}\n".format( f[dofs[0]], f[dofs[1]], 0))


        file.writelines(["CELL_DATA {}\n".format(len(mesh.elems)),
                         "TENSORS strain float\n"])
        sigma = []
        for elem in mesh.elems:
            xe = elem.xe()
            x = mean(xe[:,0])
            y = mean(xe[:,1])
            epsilon = elem.dNe([x, y]).dot(u[elem.dofs()])
            file.writelines([
                "{} {} {}\n".format(epsilon[0], epsilon[2], 0),
                "{} {} {}\n".format(epsilon[2], epsilon[1], 0),
                "{} {} {}\n".format(0, 0, 0)
            ])

            s = model._stiffness(elem.material['E'], elem.material['v']).dot(epsilon)
            sigma.append(s)

        file.write("TENSORS stress float\n")
        for s in sigma:
            file.writelines([
                "{} {} {}\n".format(s[0], s[2], 0),
                "{} {} {}\n".format(s[2], s[1], 0),
                "{} {} {}\n".format(0, 0, 0)
            ])

        file.writelines(["SCALARS von-mises float\n",
                         "LOOKUP_TABLE default\n"])
        for s in sigma:
            seq = (s[0]**2 - s[0]*s[1] + s[1]**2 + 3*s[2]**2)**0.5
            file.write("{}\n".format(seq))

    print("Done\n")
