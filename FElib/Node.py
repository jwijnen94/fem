""" Module: FElib
Node class
----------.
attributes:
    ID: (int) The ID number of the node.
    x: (numpy.ndarray) Position vector of the node.
    dofs:(numpy.ndarray) Array with the global dof numbers.
methods:
    __init__(ID, x, dofs)
    __len__(): Returns the number of dofs of the node.
"""

# Import module dependencies
from numpy import ndarray

# ------------------------------------------------------------------------------
class Node:

    def __init__(self, ID, x, dofs):
        assert isinstance(ID, int), "`ID` must be an integer."
        assert isinstance(x, ndarray), "`x` must be a numpy.ndarray"
        assert isinstance(dofs, ndarray), "`dofs` must be a numpy.ndarray"
        self.ID = ID
        self.x = x
        self.dofs = dofs


    def __len__(self):
        return len(self.dofs)
