""" Module: FElib
Element class
-------------
attributes:
    nodes: (list) Contains the nodes of the element.
    material: (dictionary) Contains the material properties of the element.
    _master: (object) Object of the master element
methods:
    __init__(ID, nodes, elem_type, material)
    __len__(): Returns the number of nodes of the element.
    __iter__(): Iterates over the nodes of the element.
    __getitem__(i): Returns the i-th node of the mesh.
    xe(): Returns the element position vector.
    ndofs(): Returns the number of dofs of the element.
    dofs(): Returns an array with the dof numbers.
    Ne(): Returns the shape functions.
    dNe(): Returns the derivatives of the shape functions.
    J(): Returns the Jacobian matrix.
    intScheme(): Returns the integration scheme of the form (xk, wk).
"""
# ------------------------------------------------------------------------------

from numpy import array, zeros
from numpy.linalg import solve

from .elements import getMaster

# ------------------------------------------------------------------------------

class Element:

    def __init__(self, ID, nodes, elem_type, material):
        assert isinstance(ID, int), "`ID` must be an integer."
        assert isinstance(nodes, list), "`nodes` must be a list."
        assert isinstance(material, dict), "`materials` must be a dictionay."
        self.ID = ID
        self.nodes = nodes
        self.type = elem_type
        self.material = material

        self._master = getMaster(elem_type)
        assert len(self.nodes) == self._master.nnodes, "Number of nodes is not corresponding with the master element."


    def __len__(self):
        return len(self.nodes)


    def __iter__(self):
        return iter(self.nodes)

    def xe(self):
        return array([node.x for node in self.nodes])


    def ndofs(self):
        return len(self.nodes) * len(self.nodes[0].dofs)


    def dofs(self):
        return array([node.dofs for node in self.nodes]).flatten()


    def Ne(self, xi):
        N = self._master.N(xi)
        Ne = zeros((2, 2*len(N)))
        for i in range(len(N)):
            Ne[0, i*2] = N[i]
            Ne[0, i*2+1] = N[i]
        return Ne


    def dNe(self, xi):
        dN = solve(self.J(xi), self._master.dN(xi).T)
        dNe = zeros((3, 2*len(dN[0])))
        for i in range(len(dN[0])):
            dNe[0, i*2] = dN[0, i]
            dNe[1, i*2+1] = dN[1, i]
            dNe[2, i*2] = dN[1, i]
            dNe[2, i*2+1] = dN[0, i]
        return dNe

    def J(self, xi):
        return self._master.dN(xi).T.dot(self.xe())


    def intScheme(self):
        return self._master.int_scheme




class ElementTF(Element):

    def __init__(self, ID, nodes, nodes2, elem_type, material):
        super().__init__(ID, nodes, elem_type, material)
        self.nodes2 = nodes2

    def ndofs2(self):
        return len(self.nodes2)


    def dofs2(self):
        return array([node.dofs2 for node in self.nodes2]).flatten()


    def Ne2(self, xi):
        return self._master.N2(xi)

    def dNe2(self, xi):
        return solve(self.J(xi), self._master.dN2(xi).T)
