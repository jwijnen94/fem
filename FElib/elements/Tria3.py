""" Module: FElib.elements
Tria3 class
-----------
attributes:
    nnodes: (int) Number of nodes of the element.
    int_scheme: (tuple) Integration scheme of the form (xk, wk).
methods:
    N(xi): Returns the array with shape functions of the master element.
    dN(xi): Returns the array with derivatives of the shape functions of the master element.
    Ne(xi, ndofs): Returns the matrix with shape functions for every dof.
    dNe(xi, ndofs, J): Returns the matrix with derivatives of shape functions after isoparametric mapping.
"""
# ------------------------------------------------------------------------------

from numpy import array
from numpy.linalg import solve

# ------------------------------------------------------------------------------

class Tria3:

    nnodes = 3

    int_scheme = (
        array([[1./6., 1./6.],
               [2./3., 1./6.],
               [1./6., 2./3.]]),
        array([1./6., 1./6., 1./6.])
    )


    def N ( self, xi ):
        return array([
            1-xi[0]-xi[1],
            xi[0],
            xi[1]
        ])

    def dN ( self, xi ):
        return array([
            [-1.,-1.],
            [ 1., 0.],
            [ 0., 1.]
        ])
