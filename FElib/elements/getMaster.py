""" Module: FElib.elements
def getMaster(elem_type): Returns the master element class for the given elem_type.
    2: (Tria3) A three node linear triangle.
    3: (Quad4) A four node linear quadrilateral.
    10: (Quad9) A nine node quadratic quadrilateral.
    20: (Quad9_4) Two field quadratic-linear quadrilateral.
"""
# ------------------------------------------------------------------------------

from .Tria3 import Tria3
from .Quad4 import Quad4
from .Quad8 import Quad8
from .Quad9 import Quad9

# ------------------------------------------------------------------------------

elements = {
    2: Tria3(),
    3: Quad4(),
    10: Quad9(),
    16: Quad8()
}

def getMaster(elem_type):
    return elements[elem_type]
