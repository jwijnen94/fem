""" Module: FElib.elements
Quad4 class
-----------
attributes:
    nnodes: (int) Number of nodes of the element.
    int_scheme: (tuple) Integration scheme of the form (xk, wk).
methods:
    N(xi): Returns the array with shape functions of the master element.
    dN(xi): Returns the array with derivatives of the shape functions of the master element.
    Ne(xi, dofs_pn): Returns the matrix with shape functions for every dof.
    dNe(xi, dofs_pn, J): Returns the matrix with derivatives of shape functions after isoparametric mapping.
"""
# ------------------------------------------------------------------------------

from numpy import array
from math import sqrt

# ------------------------------------------------------------------------------

class Quad9:

    nnodes = 9

    int_scheme_red = (
        array([
            [-1/sqrt(3), -1/sqrt(3)],
            [1/sqrt(3), -1/sqrt(3)],
            [ 1/sqrt(3), 1/sqrt(3)],
            [ -1/sqrt(3), 1/sqrt(3)],
        ]),
        array([ 1, 1, 1, 1])
     )

    int_scheme = (
         array([
             [-sqrt(0.6), -sqrt(0.6)],
             [-sqrt(0.6), sqrt(0.6)],
             [sqrt(0.6), -sqrt(0.6)],
             [sqrt(0.6), sqrt(0.6)],
             [-sqrt(0.6), 0],
             [sqrt(0.6), 0],
             [0, -sqrt(0.6)],
             [0, sqrt(0.6)],
             [0, 0],
         ]),
         array([25/81, 25/81, 25/81, 25/81, 40/81, 40/81, 40/81, 40/81, 64/81])
      )


    def N( self, xi ):
        print("aaaaaaaaaaaa")
        return array([

        ])


    def dN( self, xi ):
        return array([
            [xi[1]*(0.5*xi[0] - 0.25)*(xi[1] - 1),      0.25*xi[0]*(xi[0] - 1)*(2*xi[1] - 1)],
            [xi[1]*(0.5*xi[0] + 0.25)*(xi[1] - 1),      0.25*xi[0]*(xi[0] + 1)*(2*xi[1] - 1)],
            [xi[1]*(0.5*xi[0] + 0.25)*(xi[1] + 1),      0.25*xi[0]*(xi[0] + 1)*(2*xi[1] + 1)],
            [xi[1]*(0.5*xi[0] - 0.25)*(xi[1] + 1),      0.25*xi[0]*(xi[0] - 1)*(2*xi[1] + 1)],
            [1.0*xi[0]*xi[1]*(-xi[1] + 1),              -0.5*(xi[0]**2 - 1)*(2*xi[1] - 1)],
            [-(1.0*xi[0] + 0.5)*(xi[1]**2 - 1),         -1.0*xi[0]*xi[1]*(xi[0] + 1)],
            [-1.0*xi[0]*xi[1]*(xi[1] + 1),              -0.5*(xi[0]**2 - 1)*(2*xi[1] + 1)],
            [-(1.0*xi[0] - 0.5)*(xi[1]**2 - 1),         1.0*xi[0]*xi[1]*(-xi[0] + 1)],
            [2*xi[0]*(xi[1]**2 - 1),                    2*xi[1]*(xi[0]**2 - 1)]
        ])
