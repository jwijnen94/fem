""" Module: FElib.elements
Quad4 class
-----------
attributes:
    nnodes: (int) Number of nodes of the element.
    int_scheme: (tuple) Integration scheme of the form (xk, wk).
methods:
    N(xi): Returns the array with shape functions of the master element.
    dN(xi): Returns the array with derivatives of the shape functions of the master element.
    Ne(xi, dofs_pn): Returns the matrix with shape functions for every dof.
    dNe(xi, dofs_pn, J): Returns the matrix with derivatives of shape functions after isoparametric mapping.
"""
# ------------------------------------------------------------------------------

from numpy import array
from numpy.linalg import solve
from math import sqrt

# ------------------------------------------------------------------------------

class Quad4:

    nnodes = 4

    int_scheme = (
        array([
            [-1/sqrt(3), -1/sqrt(3)],
            [1/sqrt(3), -1/sqrt(3)],
            [ 1/sqrt(3), 1/sqrt(3)],
            [ -1/sqrt(3), 1/sqrt(3)],
        ]),
        array([ 1, 1, 1, 1])
     )


    def N( self, xi ):
        return array([
            0.25*(1-xi[0])*(1-xi[1]),
            0.25*(1+xi[0])*(1-xi[1]),
            0.25*(1+xi[0])*(1+xi[1]),
            0.25*(1-xi[0])*(1+xi[1])
        ])


    def dN( self, xi ):
        return array([
            [-0.25*(1-xi[1]), -0.25*(1-xi[0])],
            [0.25*(1-xi[1]), -0.25*(1+xi[0])],
            [0.25*(1+xi[1]), 0.25*(1+xi[0])],
            [-0.25*(1+xi[1]), 0.25*(1-xi[0])]
        ])
