""" Module: FElib
class Mesh
----------
attributes:
    elems: (list) All elements of the mesh.
    nodes: (list) All nodes of the mesh.
methods:
    __init__(elems, nodes)
    __len__(): Returns the number of elements in the mesh.
    __iter__(): Iterates over the elements in the mesh.
    __getitem__(i): Returns the i-th element of the mesh.
    nodfs(): Returns the total number of dofs of the mesh.
"""
# ------------------------------------------------------------------------------

class Mesh:

    def __init__(self, elems, nodes, ndofs):
        assert isinstance(elems, list), "`elems` must be a list."
        assert isinstance(nodes, list), "`nodes` must be a list."
        self.elems = elems
        self.nodes = nodes
        self.ndofs = ndofs

    def __len__(self):
        return len(self.elems)


    def __iter__(self):
        return iter(self.elems)


    def __getitem__(self, i):
        return self.elems[i]
