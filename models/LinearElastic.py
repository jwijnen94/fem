""" Module: models
class LinearElastic
-------------------
"""
# ------------------------------------------------------------------------------

from sys import stdout
from math import atan2, cos, sin, pi
from numpy import array, zeros, empty, ix_, add, diff, argwhere, split, array_split, sum
from numpy.linalg import det, norm
from scipy.sparse import lil_matrix
#from scipy.sparse.linalg import spsolve
from scikits.umfpack import spsolve
from multiprocessing import Pool

# ------------------------------------------------------------------------------

class LinearElastic:

    def __init__(self, mesh, thickness, state='plane_strain'):

        self._mesh = mesh
        self._thickness = thickness

        if state == 'plane_strain':
            self._stiffness = self._plane_strain
            self._state = 0
        elif state == 'plane_stress':
            self._stiffness = self._plane_stress
            self._state = 1

        self._ndofs = self._mesh.ndofs

        self._K = lil_matrix((self._ndofs, self._ndofs))
        self._f = zeros(self._ndofs)

        self._assemble()


    #######################
    # Boundary conditions #
    #######################

    def setDisp(self, disp):
        self._cons = zeros(self._ndofs, dtype=bool)
        self._cons[array(disp[:, 0], dtype=int)] = 1

        self._up = disp[:, 1]


    def setForce(self, force):
        if force:
            add.at(self._f, array(force[:,0], dtype=int), force[:,1])


    def setTraction(self, traction):
        for edge in traction:
            node1 = self._mesh.nodes[int(edge[0])]
            node2 = self._mesh.nodes[int(edge[1])]

            L = norm(node2.x - node1.x)
            theta = atan2(node2.x[1]-node1.x[1], node2.x[0]-node1.x[0]) - 0.5*pi

            if len(edge) == 4:
                p = L*edge[2]*self._thickness
                tau = L*edge[3]*self._thickness

                self._f[node1.dofs[0]] += 0.5 * (cos(theta)*p + sin(theta)*tau)
                self._f[node2.dofs[0]] += 0.5 * (cos(theta)*p + sin(theta)*tau)
                self._f[node1.dofs[1]] += 0.5 * (sin(theta)*p + cos(theta)*tau)
                self._f[node2.dofs[1]] += 0.5 * (sin(theta)*p + cos(theta)*tau)

            elif len(edge) == 5:
                node3 = self._mesh.nodes[int(edge[2])]
                p = L*edge[3]*self._thickness
                tau = L*edge[4]*self._thickness

                self._f[node1.dofs[0]] += 1/6 * (cos(theta)*p + sin(theta)*tau)
                self._f[node2.dofs[0]] += 1/6 * (cos(theta)*p + sin(theta)*tau)
                self._f[node3.dofs[0]] += 2/3 * (cos(theta)*p + sin(theta)*tau)
                self._f[node1.dofs[1]] += 1/6 * (sin(theta)*p + cos(theta)*tau)
                self._f[node2.dofs[1]] += 1/6 * (sin(theta)*p + cos(theta)*tau)
                self._f[node3.dofs[1]] += 2/3 * (sin(theta)*p + cos(theta)*tau)


    ##########################
    # Assembling and solving #
    ##########################

    def solve(self):
        print("{:25}".format("Solving system..."), end=""); stdout.flush()

        u = empty(self._ndofs)
        u[self._cons] = self._up

        # convert to csc for slicing and solving
        self._K = self._K.tocsr()

        # partitioning
        Kpp = self._K[self._cons]
        Kpp = Kpp[:, self._cons]
        Kff = self._K[~self._cons]
        Kff = Kff[:, ~self._cons]
        Kfp = self._K[~self._cons]
        Kfp = Kfp[:, self._cons]

        # Solve for unknown displacements
        u[~self._cons] = spsolve(Kff, self._f[~self._cons] - Kfp.dot(self._up))
        # Solve for unknown nodal forces
        self._f[self._cons] = Kpp.dot(self._up) + Kfp.T.dot(u[~self._cons])

        print("Done\n"); stdout.flush()

        return u, self._f


    def _assemble(self):
        print("{:25}".format("Assembling system..."), end=""); stdout.flush()

        elem_chunks = array_split(array(self._mesh.elems), 4)
        p = Pool(4)
        results = p.map(self._assembleElems, elem_chunks)
        p.close()
        p.join()

        self._K = sum(results, axis=0)
        #for result in results:
        #    self._K += result

        print("Done\n"); stdout.flush( )


    def _assembleElems(self, elems):
        K = lil_matrix((self._ndofs, self._ndofs))

        # Loop over elements
        for elem in elems:
            dofs = elem.dofs()
            Ke = zeros((len(dofs), len(dofs)))

            mat = elem.material
            C  = self._stiffness(mat['E'], mat['v'])

            # Loop over integration points
            xi, w = elem.intScheme()
            for xik, wk in zip(xi, w):

                dNe = elem.dNe(xik)
                J = elem.J(xik)

                Ke += wk * dNe.T.dot(C).dot(dNe) * det(J)

            K[ix_(dofs, dofs)] += Ke

        return K


    ######################
    # Stiffness matrices #
    ######################

    def _plane_strain(self, E, v):
        return E/(1+v)/(1-2*v) * array([
            [1-v, v,   0],
            [v,   1-v, 0],
            [0,   0,   0.5-v]
        ]) * self._thickness


    def _plane_stress( self, E, v ):
        return E / (1-v**2) * array([
            [1, v, 0],
            [v, 1, 0],
            [ 0, 0, 0.5*(1-v)]
        ]) * self._thickness
